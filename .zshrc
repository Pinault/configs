# Loads brew
eval "$(/opt/homebrew/bin/brew shellenv)"

# initialize starship
eval "$(starship init zsh)"

# initialize zoxide
eval "$(zoxide init zsh)"

# initialize asdf
. /opt/homebrew/opt/asdf/libexec/asdf.sh

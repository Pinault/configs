#!/bin/bash
set -u

# Install base packages
apt install btop mtr ripgrep procs dust starship tokei tealdeer bandwhich grex zoxide

# Install asdf
brew install asdf

echo -e '\n. $HOME/.asdf/asdf.sh' >> ~/.bash_profile
source ~/.bash_profile

asdf plugin add erlang
asdf plugin add elixir

asdf install erlang 26.2.1
asdf install elixir 1.16.1-otp-26

# Copy config files
# Github
cp gitconfig ~/.gitconfig
echo "done!"

#!/bin/bash
set -u

# Install Brew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

# Install base packages
brew install asdf btop mtr ripgrep procs dust starship tokei tealdeer bandwhich grex zoxide espanso sqlite yamlfmt stern

asdf plugin add erlang
asdf plugin add elixir

asdf install erlang 27.2.1
asdf install elixir 1.18.1-otp-27

# Copy config files
# Github
cp gitconfig ~/.gitconfig
echo "done!"
